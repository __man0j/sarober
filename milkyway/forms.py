from django import forms
from haystack.forms import SearchForm

class CustomSearchForm(SearchForm):
    prop_type = forms.ChoiceField(choices= (
        ('all','all'),
        ('land', 'Land'),
        ('building', 'Building'),
        ('plot', 'Plot'),
        ('room', 'Room')
    ))
    def search(self):
        # First, store the SearchQuerySet received from other processing.
        sqs = super(CustomSearchForm, self).search()

        if not self.is_valid():
            return self.no_query_found()

        return sqs


