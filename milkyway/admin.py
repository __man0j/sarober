from django.contrib import admin

from .models import *

# Register your models here.
admin.site.register(Estate)
admin.site.register(Position)
admin.site.register(Features)
admin.site.register(Location)
admin.site.register(Image)
admin.site.register(Owner)
admin.site.register(Plot)
admin.site.register(Housing)
admin.site.register(Apartment)
