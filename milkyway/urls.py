from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<property_id>[0-9]+)/$', views.detail, name='detail'),
#    url(r'^detail/$', views.detail, name='detail'),
    url(r'^(?P<property_id>[0-9]+)/gallery$', views.gallery, name='gallery'),
    url(r'^mapsearch/$', views.map_search, name='mapsearch'),

    url(r'^custom_search/$', views.custom_search, name='custom_search'),
    url(r'^sell/$', views.sell, name='sell'),
    url(r'^buy/$', views.buy, name='buy'),
#    url(r'^rent/$', views.rent, name='rent'),
    url(r'^contact_us/$', views.contact_us, name='contact_us'),
    url(r'^more/(?P<pid>[0-9]+)/$', views.plot, name='see_more'),
    url(r'^mindex', views.mindex,name='mindex')


]
if settings.DEBUG:
    urlpatterns+= static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

