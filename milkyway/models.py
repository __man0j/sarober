from __future__ import unicode_literals

from decimal import Decimal
from django.db import models
import os

# Create your models here.

TYPES=(
    ('building','building'),
    ('land','land'),
    ('rentroom','rentroom'),
#    ('plot','plot'),
#    ('housing','housing')
#   ('office','office')
#   ('plot','plot')
)
FOR=(
    ('sale','sale'),
    ('rent','rent'),
)

class Position(models.Model):
    lat         = models.FloatField(null=True,blank=True,default=0.0)
    lng         = models.FloatField(null=True,blank=True,default=0.0)

    def get_position(self):
        return [self.lat,self.lng]
    def __str__(self):
        return str(self.lat)+ ','+str(self.lng)


class Location(models.Model):
    zone        = models.CharField(max_length=20)
    district    = models.CharField(max_length=20)
    city        = models.CharField(max_length=20,null=True)
    locality    = models.CharField(max_length=20)
    position    = models.OneToOneField('Position')


    def __str__(self):
        return str(self.city)+', '+str(self.locality)



def get_img_path(instance,filename):
    return os.path.join('images/property_img', filename)


class Image(models.Model):
    imgfile     = models.ImageField(upload_to= get_img_path )

class Gallery(models.Model):
    imgfile     = models.ImageField(upload_to= get_img_path )
    for_estate  = models.ForeignKey('Estate',null=True,blank=True)



class Features(models.Model):
    title       = models.CharField(max_length=16,null=True)
    detail      = models.CharField(max_length=128,null=True)

    for_estate  = models.ForeignKey('Estate',null=True,blank=True)

    def __str__(self):
        return self.title+' '+ self.detail

# Purpose
# Number of Rooms
# Number of Storey

class Owner(models.Model):
    name        = models.CharField(max_length=64)
    address     = models.CharField(max_length=64,blank=True)
    profession  = models.CharField(max_length=64,blank=True)
    email       = models.EmailField(max_length=64,blank=True)
    phone       = models.CharField(max_length=64)
    img         = models.ForeignKey('Image')

    def __str__(self):
        return self.name

class Estate(models.Model):

    title       = models.CharField(max_length=32,null=True)
    ptype       = models.CharField(choices=TYPES,max_length=16)
    location    = models.ForeignKey('Location')
    price       = models.DecimalField(max_digits=10,decimal_places=0)
#    price_in_words= models.CharField(max_length=32,null=True,blank=True)
    description = models.TextField()
    is_sold     = models.BooleanField(default=False)
    is_for      = models.CharField(choices=FOR,max_length=16,default='sale')
    area        = models.CharField(max_length=32,null=True)
    img         = models.ForeignKey('Image')
    owner       = models.ForeignKey('Owner')

    position    = models.OneToOneField('Position')
    inplot      = models.ForeignKey('Plot',null=True,blank=True)
    inapartment = models.ForeignKey('Apartment',null=True,blank=True)
    inhousing   = models.ForeignKey('Housing',null=True,blank=True)

    def sold(self):
        is_sold = True

    def __str__(self):
        return 'RealEstate '+str(self.id)

class Plot(models.Model):
    title       = models.CharField(max_length=20,blank=True,null=True)
    plan_img    = models.ForeignKey('Image')
    def __str__(self):
        return 'Plot '+self.title

class Housing(models.Model):
    title       = models.CharField(max_length=20,blank=True,null=True)
    plan_img    = models.ForeignKey('Image')
    storey      = models.IntegerField(default=1)
    bhk         = models.IntegerField(default=1)

    def __str__(self):
        return 'Housing '+self.title

class Apartment(models.Model):
    title       = models.CharField(max_length=20,blank=True,null=True)
    plan_img    = models.ForeignKey('Image')
    bhk         = models.IntegerField(default=1)

    def __str__(self):
        return 'Apartment '+self.title

