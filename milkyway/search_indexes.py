from haystack import indexes
from milkyway.models import Location


class LocationIndex(indexes.SearchIndex,indexes.Indexable):
    text    = indexes.EdgeNgramField(document=True,use_template=True)
    city= indexes.CharField(null=True, model_attr='city')
    localiy= indexes.CharField(null=True,model_attr='locality')

    def get_model(self):
        return Location

    def index_queryset(self, using=None):
        return self.get_model().objects.all()

