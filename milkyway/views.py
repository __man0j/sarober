from django.shortcuts import render

from django.http import HttpResponse
from django.contrib import messages
from milkyway.models import *

from django.core.mail import get_connection, EmailMultiAlternatives, send_mail
from django.conf import settings
from milkyway.forms import CustomSearchForm
from django.core import serializers
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from haystack.views import basic_search
from haystack.query import EmptySearchQuerySet

# Create your views here.
RESULTS_PER_PAGE = getattr(settings, 'HAYSTACK_SEARCH_RESULTS_PER_PAGE', 10)

def plot(pid):
    p=Plot.objects.get(pk=pid)
    e=Estate.objects.filter(inplot=p).all()
    context={'pestate':e}
    return context

def housing(pid):
    p=Housing.objects.get(pk=pid)
    e=Estate.objects.filter(inhousing=p).all()
    context={'hestate':e}
    return context

def apartment(pid):
    p=Apartment.objects.get(pk=pid)
    e=Estate.objects.filter(inapartment=p).all()
    context={'aestate':e}
    return context


## our index page

def index(request):
    estate=Estate.objects.all()[:9]
#   take just 3 properties for index
    hot = Plot.objects.all()[0]  # get a hot aggrigated property
    items=plot(hot.id)           # get all estates in hot property

    latest={'estate':estate}
    latest['hot']=hot
    latest['form'] = CustomSearchForm
    latest.update(items)

    return render(request,'milkyway/index.html', latest)


def buy(request):
    estate=Estate.objects.all()
    paginator = Paginator(estate,RESULTS_PER_PAGE)
    page = paginator.page(int(request.GET.get('page', 1)))

    return render(request,'milkyway/properties.html',{'estate':page})



def mindex(request):
    estate=Estate.objects.all()[:]
#   take just 3 properties for index
    latest={'estate':estate}
    latest['form'] = CustomSearchForm

    return render(request,'milkyway/mindex.html', latest)


def see_more(request):
    prop=Estate.objects.all()
    content={'prop':prop}

    return render(request,'milkyway/seeall.html', content)

def queryset_gen(search_qs):
    for item in search_qs:
        yield item.object  # This is the line that gets the model instance out of the Search object

def map_search(request):

    query = ''
    results = EmptySearchQuerySet()
    prop_type=''

    if request.GET.get('q'):
        form = CustomSearchForm(request.GET, load_all=True)

        if form.is_valid():
            query = form.cleaned_data['q']
            prop_type = form.cleaned_data['prop_type']
            results = form.search()
    else:
        form = CustomSearchForm(load_all=True)
    locations=[]
    result=queryset_gen(results)
    for x in result:
        locations.append(x)

    print(locations)
    context={'locations':locations ,'ptype':prop_type}
    if locations:
        context['center']=locations[0]
    else:
        return HttpResponse("No Properties Found!!!")

    return render(request,'milkyway/mapsearch.html',context)

def detail(request,property_id):

    prop=Estate.objects.get(id=property_id)

    cord={'lat':prop.location.position.lat,'lng':prop.location.position.lng}
    cord['prop']=prop
    print(cord)
    return render(request,'milkyway/detail.html',cord)



def sell(request):
    if request.method== 'POST':
        name=request.POST['owner']
        num=request.POST['num']
        address=request.POST['address']
        mailaddress=request.POST['email']
        location =request.POST['location']
        price = request.POST['price']
        area=request.POST['area']
        isfor=request.POST['ptype']
        about=request.POST['about']

        if name=='' or num=='' or name==' ' or num==' ':
            messages.warning(request,'Please recheck form again and try !!!')


        subject,seller,to='Sell Property','mail@sarober.com','manesarober@gmail.com'
        html_content='<h2>New Propery '+ isfor +' request:</h2> <p>owner:'+name+'<br>'+'phone num:'+num+'<br>'+ 'address:'+address+'<br>'+ 'mailaddress:'+mailaddress+'<br>'+ 'location:'+location+'<br>'+ 'price:'+price+'<br>'+ 'area:'+area+'<br>'+ 'description:'+about+'</p>'

        email=EmailMultiAlternatives(subject,'''I wanna sell this propery''',seller,[to,'bb.bindash35@gmail.com'])
        email.attach_alternative(html_content,'text/html')
        email.send()
#        send_mail(subject,description,'mail@sarober.com',['manesarober@gmail.com'], fail_silently=False,)

        context="Success !! Your sell request is send. We will Contact you as soon as possibe."
        messages.success(request,context)
    return render(request,'milkyway/sell.html')

def contact_us(request):
    if request.method== 'POST':
        name=request.POST['owner']
        num=request.POST['num']
        address=request.POST['address']
        mailaddress=request.POST['email']
        about=request.POST['about']

        if name=='' or num=='' or name==' ' or num==' ':
            messages.warning(request,'Please recheck form again and try !!!')


        subject,seller,to='Message','mail@sarober.com','manesarober@gmail.com'
        html_content='<h2>New Message request:</h2> <p>Name: '+name+'<br>'+'phone num:'+num+'<br>'+ 'address:'+address+'<br>'+ 'mailaddress:'+mailaddress+'<br>'+ 'description:'+description+'</p>'

        email=EmailMultiAlternatives(subject,'''I wanna sell this propery''',seller,[to,'bb.bindash35@gmail.com'])
        email.attach_alternative(html_content,'text/html')
        email.send()
#        send_mail(subject,description,'mail@sarober.com',['manesarober@gmail.com'], fail_silently=False,)

        messages.success(request,'Thanks For Your Message')
    return render(request,'milkyway/contactus.html')




def gallery(request,property_id):
    return HttpResponse("You're looking at gallery %s" % property_id)


def custom_search(request):
    query = ''
    results = EmptySearchQuerySet()
    prop_type=''

    if request.GET.get('q'):
        form = CustomSearchForm(request.GET, load_all=True)

        if form.is_valid():
            query = form.cleaned_data['q']
            prop_type = form.cleaned_data['prop_type']
            results = form.search()
    else:
        form = CustomSearchForm(load_all=True)

    print (query)

    paginator = Paginator(results,RESULTS_PER_PAGE)

    try:
        page = paginator.page(int(request.GET.get('page', 1)))
    except InvalidPage:
        raise HttpResponse("No such page of results!")

    context = {
        'form': form,
        'page': page,
        'paginator': paginator,
        'query': query,
        'suggestion': None,
        'prop_type':prop_type,
    }

    if results.query.backend.include_spelling:
        context['suggestion'] = form.get_suggestion()


    return render(request,'milkyway/searchresults.html', context)

